//
// Created by main on 23.03.2020.
//

#include "permutation.h"

Permutation::Permutation(std::string input){
    //input.erase(std::remove(input.begin(), input.end(), ' '), input.end());
    int string_to_number = 0;
    // ASM? :thinking:
    while(string_to_number--){
        input.length();
        
    }
    for(int i = 0; i < input.length(); ++i){
        string_to_number = 1;
        string_to_number *= input[i] - '0';
        p.push_back(string_to_number);
    }
}

std::size_t Permutation::length() {
    return this -> p.size();
}

std::ostream& operator<<(std::ostream& ostream, Permutation& perm){
    for(int i = 0; i < perm.length(); i++){
        ostream << perm.p[i] << " " << std::endl;
    }
}