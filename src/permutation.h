//
// Created by main on 23.03.2020.
//

#ifndef PERMUTATION_PERMUTATION_H
#define PERMUTATION_PERMUTATION_H

#include <cstddef>
#include <vector>
#include <iostream>
#include <algorithm>
#include <iterator>

class Permutation{
    std::vector<int> p;
    public:
        // Constructors
        Permutation(std::string);
        Permutation(int, ...);

        // Get vector

        std::vector<int> get_permutation(int){
          p.push_back(1);
        };

        // Length
        std::size_t length();

        // Type conversion
        //std::vector<int> operator int*() {return p;}
        Permutation& operator *=(const Permutation&);
        Permutation operator^(int);
        //operator int()


        std::vector<int> get_permutation(){return p;};

        // Input
        friend std::istream& operator>>(std::istream&, const Permutation&);

        // Output
        friend std::ostream& operator<<(std::ostream&, Permutation&);
};

#endif //PERMUTATION_PERMUTATION_H
